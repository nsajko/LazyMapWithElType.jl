using Test, LazyMapWithElType

const map_i = LazyMapWithElType.map_with_inferred_eltype

@testset "LazyMapWithElType.jl" begin
  @test isempty(detect_ambiguities(LazyMapWithElType, recursive = true))
  @test isempty(detect_unbound_args(LazyMapWithElType, recursive = true))

  @test eltype(map_i((x -> x^3), 1:5)) <: Int
  @test eltype(map_i(sqrt, 1:5)) <: Float64

  @test collect(map_i((x -> x^2), 1:50)) == map((x -> x^2), 1:50)

  @test (
    collect(Iterators.reverse(map_i((x -> x^2), 1:50))) ==
    reverse(map((x -> x^2), 1:50))
  )

  @test collect(map_i(+, 1:50, 301:350)) == map(+, 1:50, 301:350)
end
