# LazyMapWithElType

A lazy map, an alternative for `Base.Generator` and `Base.Iterators.map`. The `eltype` of
the produced iterators is stored as a type parameter. The element type may be provided
explicitly or inferred automatically.

## An alternative package: `FlexiMaps`

The `FlexiMaps` package is more fully featured, with its `mapview` function providing
functionality that's very similar or equivalent to the functions of this package.

JuliaHub: https://juliahub.com/ui/Packages/General/FlexiMaps

GitLab: https://gitlab.com/aplavin/FlexiMaps.jl
