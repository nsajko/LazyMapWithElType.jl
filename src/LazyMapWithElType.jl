# Copyright © 2023 Neven Sajko

module LazyMapWithElType

struct Generator{Eltype, Map, Iterator}
  f::Map
  iter::Iterator
end

Base.eltype(::Generator{E}) where {E} = E
Base.first(g::Generator) = (g.f)(first(g.iter))
Base.length(g::Generator) = length(g.iter)
Base.size(g::Generator) = size(g.iter)
Base.axes(g::Generator) = axes(g.iter)
Base.ndims(g::Generator) = ndims(g.iter)
Base.keys(g::Generator) = keys(g.iter)
Base.last(g::Generator) = (g.f)(last(g.iter))
Base.isempty(g::Generator) = isempty(g.iter)
Base.isdone(g::Generator) = isdone(g.iter)
Base.isdone(g::Generator, state) = isdone(g.iter, state)
Base.IteratorSize(::Type{<:Generator{<:Any,<:Any,I}}) where {I} = Base.IteratorSize(I)

struct NoState end
iterate_helper(it, ::NoState) = iterate(it)
iterate_helper(it, state) = iterate(it, state)

function Base.iterate(g::Generator{E}, state = NoState()) where {E}
  y = iterate_helper(g.iter, state)
  if isnothing(y)
    nothing
  else
    (x, s) = y
    (((g.f)(x))::E, s)
  end
end

generator(::Type{E}, f::M, iter::I) where {E,M,I} = Generator{E,M,I}(f, iter)
generator(::Type{E}, ::Type{M}, iter::I) where {E,M,I} = Generator{E,Type{M},I}(M, iter)

Base.Iterators.reverse(g::Generator) = generator(eltype(g), g.f, reverse(g.iter))

zipped(f::M, iters...) where {M} = (
  let f = f
    a -> f(a...)
  end,
  zip(iters...)
)

"""
Like `Iterators.map` from Julia's `Base`, but also takes a parameter that
determines the `eltype` of the resulting iterator.
"""
function map_with_eltype end

map_with_eltype(::Type{E}, f::M, iter) where {E,M} = generator(E, f, iter)

map_with_eltype(::Type{E}, f::M, i0, i1, is...) where {E,M} = generator(
  E,
  zipped(f, i0, i1, is...)...,
)

"""
Tries to infer the `eltype` of the iterator resulting from applying `f` to
`iter`.

!!! warning
    The inference is costly from a performance perspective.

!!! warning
    This relies on the `Base.return_types` function that isn't part of Julia's
    public interface. Thus updates of Julia may break `infer_eltype_of_map`.
"""
infer_eltype_of_map(f::M, iter) where {M} =
  Union{Base.return_types(f, Tuple{eltype(iter)})...}

"""
Like `Iterators.map` from Julia's `Base`, but it tries to infer the `eltype`
of the resulting iterator using `infer_eltype_of_map`.

!!! warning
    The inference is costly from a performance perspective.

!!! warning
    `infer_eltype_of_map` relies on the `Base.return_types` function that
    isn't part of Julia's public interface. Thus updates of Julia may cause
    breakage.
"""
function map_with_inferred_eltype end

map_with_inferred_eltype(f::M, iter) where {M} =
  generator(infer_eltype_of_map(f, iter), f, iter)

map_with_inferred_eltype(f::M, i0, i1, is...) where {M} =
  map_with_inferred_eltype(zipped(f, i0, i1, is...)...)

end
